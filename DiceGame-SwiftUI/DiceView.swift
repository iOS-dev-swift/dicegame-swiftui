//
//  DiceView.swift
//  DiceGame-SwiftUI
//
//  Created by Cohen, Dor on 18/10/2020.
//

import SwiftUI

struct DiceView: View {
    let diceNumber: Int
    var body: some View {
        Image("dice\(diceNumber)")
            .resizable()
            .aspectRatio(1, contentMode: .fit)
            .padding(.all, 40.0)
    }
}

struct DiceView_Previews: PreviewProvider {
    static var previews: some View {
        DiceView(diceNumber: 1).previewLayout(.sizeThatFits)
    }
}
