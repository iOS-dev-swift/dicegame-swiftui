//
//  DiceGame_SwiftUIApp.swift
//  DiceGame-SwiftUI
//
//  Created by Cohen, Dor on 18/10/2020.
//

import SwiftUI

@main
struct DiceGame_SwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
